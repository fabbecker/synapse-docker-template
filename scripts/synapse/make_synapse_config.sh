source ./.env
mkdir ${BASE_PATH:-./}data/synapse/ -p
# Use original config generation to get the correct file permissions
docker-compose run synapse generate
# Overwrite config contents with custom values
./scripts/synapse/generate_synapse_config.sh > ${BASE_PATH:-./}data/synapse/homeserver.yaml
