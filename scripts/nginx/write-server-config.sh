mkdir -p /etc/nginx/vhost.d/

if [ -f /etc/nginx/vhost.d/${SUBDOMAIN}.conf ]; then
  echo "" > /etc/nginx/vhost.d/${SUBDOMAIN}.conf
else
  echo "server {\n  listen       80;\n  listen  [::]:80;\n  server_name  ${SUBDOMAIN};\n  include /etc/nginx/vhost.d/${SUBDOMAIN}.conf;\n}" >> /etc/nginx/conf.d/${SUBDOMAIN}.conf
fi

echo "location /.well-known/matrix/client {\n  access_log off;\n  add_header Access-Control-Allow-Origin *;\n  default_type application/json;\n  return 200 '{\"m.homeserver\": {\"base_url\": \"https://matrix.${SUBDOMAIN}\"}, \"org.matrix.msc3575.proxy\": {\"url\": \"https://sliding-sync.${SUBDOMAIN}\"}}';\n}\n" >> /etc/nginx/vhost.d/${SUBDOMAIN}.conf
echo "location /.well-known/matrix/server {\n  access_log off;\n  add_header Access-Control-Allow-Oritin *;\n  default_type application/json;\n  return 200 '{\"m.server\": \"matrix.${SUBDOMAIN}:443\"}';\n}\n" >> /etc/nginx/vhost.d/${SUBDOMAIN}.conf
if [ ${FAVICON_URL} != "" ]; then
echo "location = /favicon.ico {\n  proxy_pass ${FAVICON_URL};\n}\n" >> /etc/nginx/vhost.d/${SUBDOMAIN}.conf
fi
if [ ${REDIRECT_URL} != "" ]; then
echo "location / {\n  return 302 ${REDIRECT_URL};\n}\n" >> /etc/nginx/vhost.d/${SUBDOMAIN}.conf
fi
