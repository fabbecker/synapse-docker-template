# Docker config to host synapse on a subdomain with sliding sync
## Requirements
This project requires a reverse proxy to be running.
You can use the recommended docker-compose file to quickly get up and running.
```
mkdir ../reverse-proxy
cp reverse-proxy-docker-compose.yml ../reverse-proxy/docker-compose.yml
# edit your email in `../reverse-proxy/docker-compose.yml` or use an environment variable
DEFAULT_MAIL=<your-email-address-here> sudo docker-compose -f ../reverse-proxy/docker-compose.yml up -d
```
If you do not want to use this reverse proxy, use `sudo docker-compose config` to take a look at which `VIRTUAL_HOST` and `VIRTUAL_PORT` to proxy_pass your requests to, after editing `.env`.

## Installation
### Edit variables
```
nano .env
```

### Optional consent tracking
If you have a policy that you want your users to consent to, save it to `data/synapse/user_consent/en/`. If the version of this policy is `1.0` save it to
```
data/synapse/user_consent/en/1.0.html
```
More [info on consent tracking](https://matrix-org.github.io/synapse/latest/consent_tracking.html).

### Creating the synapse config file:
```
./scripts/synapse/make_synapse_config.sh
```

### Starting the containers
```
docker-compose up -d
```
